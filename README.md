<div align="center">

# List of all public projects.

</div>

### Links

- [bashrc-aliases](https://gitlab.com/hstokes/bashrc-aliases)

- [mac-changer-simple](https://gitlab.com/hstokes/mac-changer-simple)

- [openpyn-simple-bash](https://gitlab.com/hstokes/openpyn-simple-bash)

- [openpyn-simple-python](https://gitlab.com/hstokes/openpyn-simple-python)

- [bash-snippets](https://gitlab.com/hstokes/bash-snippets)

- [java-snippets](https://gitlab.com/hstokes/java-snippets)